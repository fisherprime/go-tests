package main

import "fmt"

type blah struct {
	val int
}

func main() {
	array := []blah{
		{val: 1}, {val: 2}, {val: 3},
	}

	demo := make(map[int]*blah)
	demo2 := make(map[int]*blah)

	for _, v := range array {
		// Will be a pointer to the v variable; same for the loops iterations.
		demo[v.val] = &v
	}
	for index, v := range array {
		// Will be pointer to actual array entry.
		demo2[v.val] = &array[index]
	}
	for index, v := range array {
		fmt.Printf("array location: %p, val location: %p, copy location: %p\n", array, &array[index], &v)
	}

	fmt.Printf("Dumping Demo:\n%+v\n", demo)
	fmt.Printf("Dumping Demo2:\n%+v\n", demo2)
}
