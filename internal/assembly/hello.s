// SPDX-License-Identifier: MIT
#include "textflag.h"

// Table of characters; include terminating null.

// `c_array<>` defines (c-like `static`).
// One approach to storing a string.
DATA c_array+0(SB)/1, $'h'
DATA c_array+1(SB)/1, $'e'
DATA c_array+2(SB)/1, $'l'
DATA c_array+3(SB)/1, $'l'
DATA c_array+4(SB)/1, $'o'
GLOBL c_array(SB), RODATA, $6

// Another approach to storing a string.
DATA str+0(SB)/8, $" world!\n"
GLOBL str(SB), RODATA, $8

// FIXME: MOVL c_array & str values into register?
// func HelloWorld()
TEXT ·HelloWorld(SB), NOSPLIT, $0
	// 32-bit registers.
	MOVL $1, AX
	MOVL $1, DI
	MOVL $6, DX
	LEAL c_array(SB), SI
	SYSCALL

	// 64-bit registers.
	MOVQ $1, AX
	MOVQ $1, DI
	MOVQ $8, DX
	LEAQ str(SB), SI
	SYSCALL
	RET

// vim: ft=goasm
