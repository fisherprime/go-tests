// SPDX-License-Identifier: MIT
package assembly

func Multiply64Const(num int64) int64
func Multiply64(num1, num2 int64) int64

func Multiply32(num1, num2 int32) int32
