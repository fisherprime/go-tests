// SPDX-License-Identifier: MIT

// REF: https://golang.org/doc/asm, https://9p.io/sys/doc/asm.html, man (2) syscall, http://www.egr.unlv.edu/~ed/assembly64.pdf.

// NOTE:
//
// If `NOSPLIT` is not specified for `TEXT`, the argument size must be provided.
//
// $XX-YY specifies the frame size (XX) & argument size (YY) as bytes.
//
// `GLOBL` must follow `DATA`.
//
// `RODATA` is the read-only section & implicitly `NOPTR` (no need for GC).
//
// The symbol used on function definition is middle dot `·` not dot `.`

// Compiler generated include file containing offsets to Go type fields.
// #include "go_asm.h"

// Contains defintion for the `RODATA`, `NOPTR`, … flags.
#include "textflag.h"

// Global variable.
// 64-bit integer.
DATA ConstNum+0(SB)/8, $7
GLOBL ConstNum(SB), RODATA, $8

// func Multiply64Const(num int64) int64
TEXT ·Multiply64Const(SB), NOSPLIT, $16-8
	MOVQ num+0(FP), AX
	MULQ ConstNum(SB)
	MOVQ AX, ret+8(FP)
	RET

// func Multiply64(num1, num2 int64) int64
TEXT ·Multiply64(SB), NOSPLIT, $24-8
	MOVQ num1+0(FP), AX
	MOVQ num2+8(FP), BX
	IMULQ BX
	MOVQ AX, ret+16(FP)
	RET

// NOTE: 32-bit * 32-bit = 64-bit
// func Multiply32(num1, num2 int32) int32
TEXT ·Multiply32(SB), NOSPLIT, $16-4
	MOVL num1+0(FP), AX
	MOVL num2+4(FP), BX
 	IMULL BX
	MOVL AX, ret+8(FP)
    RET

// vim: ft=goasm
