// SPDX-License-Identifier: MIT
package maps

var (
	testLUT = map[int]string{
		1:  "one",
		2:  "two",
		3:  "three",
		4:  "four",
		5:  "five",
		6:  "six",
		7:  "seven",
		8:  "eight",
		9:  "nine",
		10: "ten",
	}
)

func readLUT(key int) string { return testLUT[key] }
