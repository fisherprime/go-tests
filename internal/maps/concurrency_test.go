// SPDX-License-Identifier: MIT
package maps

import (
	"testing"
)

func Benchmark_readLUT(b *testing.B) {
	lutLimit := len(testLUT)

	/* b.RunParallel(func(pb *testing.PB) {
	 *     for pb.Next() { */
	for index := 1; index <= lutLimit; index++ {
		go b.Log(index, "->", readLUT(index))
	}
	/*     }
	 * }) */
}
