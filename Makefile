-include .env

# VERSION := $(shell git describe --tags)
# BUILD := $(shell git rev-parse --short HEAD)
PROJECTNAME := $(shell basename "$(PWD)")

# Go related variables.
BASE_DIR := $(shell pwd)
CMD_DIR := $(BASE_DIR)/cmd
BUILD_DIR := $(BASE_DIR)/build

# # Use linker flags to provide version/build settings
VERSION := $(shell git describe --tags)
BUILD := $(shell date --rfc-3339=seconds)

# Redirect error output to a file, so we can show it in development mode.
STDERR := /tmp/$(PROJECTNAME)-make.log

# PID file will keep the process id of the server
PID := /tmp/.$(PROJECTNAME).pid

# `make` is verbose in Linux; this reduces the verbosity.
MAKEFLAGS += --silent -j$(shell nproc)

# Set `GOPATH` to the variable name if it does not exist.
GOPATH ?= "$$GOPATH"

.PHONY: build
build:
	$(BASE_DIR)/scripts/build.sh $(BUILD_DIR) $(VERSION) $(BUILD)

.PHONY: dep
dep:
	@printf "  >  Updating dependencies\n"
	go mod tidy

.PHONY: install
install:
	$(BASE_DIR)/scripts/install.sh $(VERSION) $(BUILD)

.PHONY: clean
clean:
	@printf "  >  Cleaning $(BUILD_DIR)\n"
	rm -rf $(BUILD_DIR)

.PHONY: help
help: Makefile
	@printf "BUILD_DIR:\t$(BUILD_DIR)\n"
	@printf "INSTALL_DIR:\t$(GOPATH)/bin\n\n"
	@
	@printf "Choose an operation for $(PROJECTNAME):\n\n"
	@
	@printf "build:\t\tBuild the $(PROJECTNAME) binaries\n"
	@printf "clean:\t\tClean the $(PROJECTNAME) BUILD_DIR\n"
	@printf "dep:\t\tUpdate dependencies\n"
	@printf "install:\tInstall the $(PROJECTNAME) binaries to INSTALL_DIR\n"
